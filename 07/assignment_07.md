# assignment title

## 1. Problem Definition

### (1) Code

-

### (2) Data

- 

### (3) Objective Function

- 

### (4) Optimization

- 

## 2. Submission

### (1) jupyter notebook file in `ipynb` format 

- 

- 

### (2) jupyter notebook file in `PDF` format

- export the completed jupyer notebook file to PDF format
- you try to first convert jupyter `notebook` to `HTML` and then convert `HTML` to `PDF`
- submit the PDF file from the completed jupyer notebook file

### (3) GitHub history page in `PDF` format

- make `git commit` as many as you want in such a way that you can effectively demonstrate the entire procedure in completing the code
- export the GitHub history page to PDF file
- submit the PDF file from the GitHub history page