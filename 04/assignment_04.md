## Heat equation

### (1) Baseline notebook code

- notebook: [assignment_04.ipynb](https://gitlab.com/cau-class/machine-learning/2022-1/assignment/-/blob/main/04/assignment_04.ipynb) 

### (2) Data

- input image: [barbara_color.jpeg](https://gitlab.com/cau-class/machine-learning/2022-1/assignment/-/blob/main/04/barbara_color.jpeg)

### (3) Finte difference

#### gradient $`\nabla f(x, y) = 
\begin{bmatrix}
\frac{\partial f}{\partial x}\\
\frac{\partial f}{\partial y}
\end{bmatrix}`$

- forward difference
```math
\frac{\partial f}{\partial x}(x, y) = \frac{f(x+h, y) - f(x, y)}{h}
```

```math
\frac{\partial f}{\partial y}(x, y) = \frac{f(x, y+h) - f(x, y)}{h}
```

- backward difference

```math
\frac{\partial f}{\partial x}(x, y) = \frac{f(x, y) - f(x-h, y)}{h}
```

```math
\frac{\partial f}{\partial y}(x, y) = \frac{f(x, y) - f(x, y-h)}{h}
```


## [Submission]

### (1) jupyter notebook file in `ipynb` format 

- download the baseline jupyter notebook to your local folder
- complete the jupyter notebook in `ipynb` format
- submit the `ipynb` file

### (2) jupyter notebook file in `PDF` format

- export the completed jupyer notebook to `PDF` format
- you can try to first convert jupyter notebook `ipynb` to `HTML` and then convert `HTML` to `PDF`
- submit the `PDF` file

### (3) GitHub history page in `PDF` format

- make `git add` the jupyter notebook file to the repository for the assignment at your github
- make `git commit -m "initial commit"` at the beginning of coding
- make `git commit -m "final commit"` at the completion of coding
- make `git commit -m "your own message"` at least 10 times in such a way that your coding procedure is effectively demonstrated
- the number of `git commit` for the jupyter notebook should be at least 12
- export the GitHub history page for the jupyter notebook to `PDF` format
- submit the `PDF` file
